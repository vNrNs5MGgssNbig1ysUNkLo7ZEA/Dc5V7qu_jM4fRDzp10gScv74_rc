Credits::
	ld a, b
	and $40
	ld [wJumptableIndex], a

	ld a, [rSVBK]
	push af
	ld a, $5
	ld [rSVBK], a

	call ClearBGPalettes
	call ClearTileMap
	call ClearSprites

	ld hl, wCreditsFaux2bpp
	ld c, $80
	ld de, $ff00

.load_loop
	ld a, e
	ld [hli], a
	ld a, d
	ld [hli], a
	dec c
	jr nz, .load_loop

	ld de, CreditsBorderGFX
	ld hl, VTiles2 tile $20
	lb bc, BANK(CreditsBorderGFX), $09
	call Request2bpp

	ld de, CopyrightGFX
	ld hl, VTiles2 tile $60
	lb bc, BANK(CopyrightGFX), $1d
	call Request2bpp

	ld de, TheEndGFX
	ld hl, VTiles2 tile $40
	lb bc, BANK(TheEndGFX), $10
	call Request2bpp

	ld a, $ff
	ld [wCreditsBorderFrame], a
	xor a
	ld [wCreditsBorderMon], a

	call Credits_LoadBorderGFX
	ld e, l
	ld d, h
	ld hl, VTiles2
	lb bc, BANK(CreditsMonsGFX), 16
	call Request2bpp

	call ConstructCreditsTilemap
	xor a
	ld [wCreditsLYOverride], a

	ld hl, wLYOverrides
	ld bc, $100
	xor a
	call ByteFill

	ld a, rSCX & $ff
	ld [wLCDCPointer], a
	ld hl, rIE
	set LCD_STAT, [hl]
	call GetCreditsPalette
	call SetPalettes
	ld a, [hVBlank]
	push af
	ld a, $5
	ld [hVBlank], a
	xor a
	ld [hBGMapMode], a
	ld [CreditsPos], a
	ld [CreditsPos + 1], a
	ld [CreditsTimer], a
	inc a
	ld [hInMenu], a

.execution_loop
	call Credits_HandleBButton
	call Credits_HandleAButton
	jr nz, .exit_credits

	call Credits_Jumptable
	call DelayFrame
	jr .execution_loop

.exit_credits
	call ClearBGPalettes
	xor a
	ld [wLCDCPointer], a
	ld [hBGMapAddress], a
	ld hl, rIE
	res LCD_STAT, [hl]
	pop af
	ld [hVBlank], a
	pop af
	ld [rSVBK], a
	ret

Credits_HandleAButton:
	ld a, [hJoypadDown]
	and A_BUTTON
	ret z
	ld a, [wJumptableIndex]
	bit 7, a
	ret

Credits_HandleBButton:
	ld a, [hJoypadDown]
	and B_BUTTON
	ret z
	ld a, [wJumptableIndex]
	bit 6, a
	ret z
	ld hl, CreditsPos
	ld a, [hli]
	cp $d
	jr nc, .okay
	ld a, [hli]
	and a
	ret z
.okay
	ld hl, CreditsTimer
	ld a, [hl]
	and a
	ret z
	dec [hl]
	ret

Credits_Jumptable:
	ld a, [wJumptableIndex]
	and $f
	jumptable

.Jumptable

	dw ParseCredits
	dw Credits_Next
	dw Credits_Next
	dw Credits_PrepBGMapUpdate
	dw Credits_UpdateGFXRequestPath
	dw Credits_RequestGFX
	dw Credits_LYOverride
	dw Credits_Next
	dw Credits_Next
	dw Credits_Next
	dw Credits_UpdateGFXRequestPath
	dw Credits_RequestGFX
	dw Credits_LoopBack


Credits_Next:
	ld hl, wJumptableIndex
	inc [hl]
	ret

Credits_LoopBack:
	ld hl, wJumptableIndex
	ld a, [hl]
	and $f0
	ld [hl], a
	ret

Credits_PrepBGMapUpdate:
	xor a
	ld [hBGMapMode], a
	jp Credits_Next

Credits_UpdateGFXRequestPath:
	call Credits_LoadBorderGFX
	ld a, l
	ld [hRequestedVTileSource], a
	ld a, h
	ld [hRequestedVTileSource + 1], a
	ld a, VTiles2 % $100
	ld [hRequestedVTileDest], a
	ld a, VTiles2 / $100
	ld [hRequestedVTileDest + 1], a
	; fallthrough

Credits_RequestGFX:
	xor a
	ld [hBGMapMode], a
	ld a, $8
	ld [hRequested2bpp], a
	jr Credits_Next

Credits_LYOverride:
	ld a, [rLY]
	cp $30
	jr c, Credits_LYOverride
	ld a, [wCreditsLYOverride]
	sub 2
	ld [wCreditsLYOverride], a
	ld hl, wLYOverrides + $1f
	call .Fill
	ld hl, wLYOverrides + $87
	call .Fill
	jr Credits_Next

.Fill
	ld c, $8
.loop
	ld [hli], a
	dec c
	jr nz, .loop
	ret

ParseCredits:
	ld hl, wJumptableIndex
	bit 7, [hl]
	jr nz, Credits_Next

; Wait until the timer has run out to parse the next command.
	ld hl, CreditsTimer
	ld a, [hl]
	and a
	jr z, .parse

; One tick has passed.
	dec [hl]
	jr Credits_Next

.parse
; First, let's clear the current text display,
; starting from line 5.
	xor a
	ld [hBGMapMode], a
	hlcoord 0, 5
	ld bc, 20 * 12
	ld a, " "
	call ByteFill

; Then read the script.

.loop
	call .get

; Commands:
	cp CREDITS_END
	jp z, .end
	cp CREDITS_WAIT
	jr z, .wait
	cp CREDITS_SCENE
	jr z, .scene
	cp CREDITS_CLEAR
	jr z, .clear
	cp CREDITS_MUSIC
	jr z, .music
	cp CREDITS_WAIT2
	jr z, .wait2
	cp CREDITS_THEEND
	jr z, .theend

; If it's not a command, it's a string identifier.

	push af
	ld e, a
	ld d, 0
	ld hl, CreditsStrings
	add hl, de
	add hl, de
	ld a, [hli]
	ld d, [hl]
	ld e, a
	pop af

; Strings spanning multiple lines have special cases.

	cp CREDITS_STAFF
	jr c, .staff

; The rest start from line 6.

	hlcoord 1, 6
	jr .print

.copyright
	hlcoord 2, 6
	jr .print

.staff
	hlcoord 1, 6

.print
; Print strings spaced every two lines.
	call .get
	ld bc, 20 * 2
	rst AddNTimes
	call PlaceText
	jr .loop

.theend
; Display "The End" graphic.
	call Credits_TheEnd
	jr .loop

.scene
; Update the scene number and corresponding palette.
	call .get
	ld [wCreditsBorderMon], a ; scene
	xor a
	ld [wCreditsBorderFrame], a ; frame
	call GetCreditsPalette
	call SetPalettes ; update hw pal registers
	jr .loop

.clear
; Clear the banner.
	ld a, $ff
	ld [wCreditsBorderFrame], a ; frame
	jr .loop

.music
; Play the credits music.
	ld de, MUSIC_CREDITS
	push de
	ld de, MUSIC_NONE
	call PlayMusic
	call DelayFrame
	pop de
	call PlayMusic
	jr .loop

.wait2
; Wait for some amount of ticks.
	call .get
	ld [CreditsTimer], a
	jr .done

.wait
; Wait for some amount of ticks, and do something else.
	call .get
	ld [CreditsTimer], a

	xor a
	ld [hBGMapHalf], a
	inc a
	ld [hBGMapMode], a

.done
	jp Credits_Next

.end
; Stop execution.
	ld hl, wJumptableIndex
	set 7, [hl]
	ld a, 32
	ld [MusicFade], a
	ld a, MUSIC_POST_CREDITS % $100
	ld [MusicFadeID], a
	ld a, MUSIC_POST_CREDITS / $100
	ld [MusicFadeIDHi], a
	ret

.get
; Get byte CreditsPos from CreditsScript
	push hl
	push de
	ld hl, CreditsPos
	push hl
	ld a, [hli]
	ld e, a
	ld d, [hl]
	ld hl, CreditsScript
	add hl, de

	ld a, [hl]
	pop hl
	inc [hl]
	jr nz, .no_carry_on_increment
	inc hl
	inc [hl]
.no_carry_on_increment
	pop de
	pop hl
	ret

ConstructCreditsTilemap:
	xor a
	ld [hBGMapMode], a
	ld a, $c
	ld [hBGMapAddress], a

	ld a, $28
	hlcoord 0, 0
	ld bc, SCREEN_HEIGHT * SCREEN_WIDTH
	call ByteFill

	ld a, $7f
	hlcoord 0, 4
	ld bc, (SCREEN_HEIGHT - 4) * SCREEN_WIDTH
	call ByteFill

	hlcoord 0, 4
	ld a, $24
	call DrawCreditsBorder

	hlcoord 0, 17
	ld a, $20
	call DrawCreditsBorder

	hlcoord 0, 0, AttrMap
	ld bc, 4 * SCREEN_WIDTH
	xor a
	call ByteFill

	hlcoord 0, 4, AttrMap
	ld bc, SCREEN_WIDTH
	ld a, $1
	call ByteFill

	hlcoord 0, 5, AttrMap
	ld bc, 12 * SCREEN_WIDTH
	ld a, $2
	call ByteFill

	hlcoord 0, 17, AttrMap
	ld bc, SCREEN_WIDTH
	ld a, $1
	call ByteFill

	call ApplyAttrAndTilemapInVBlank
	xor a
	ld [hBGMapMode], a
	ld [hBGMapAddress], a
	hlcoord 0, 0

	ld b, 5
.outer_loop
	push hl
	ld de, SCREEN_WIDTH - 3
	ld c, 4
	xor a
.inner_loop
rept 3
	ld [hli], a
	inc a
endr
	ld [hl], a
	inc a
	add hl, de
	dec c
	jr nz, .inner_loop
	pop hl
rept 4
	inc hl
endr
	dec b
	jr nz, .outer_loop
	jp ApplyAttrAndTilemapInVBlank

DrawCreditsBorder:
	ld c, SCREEN_WIDTH / 4
.loop
	push af
rept 3
	ld [hli], a
	inc a
endr
	ld [hli], a
	pop af
	dec c
	jr nz, .loop
	ret

GetCreditsPalette:
	call .GetPalAddress

	push hl
	ld a, 0
	call .UpdatePals
	pop hl
	ret

.GetPalAddress
; Each set of palette data is 24 bytes long.
	ld a, [wCreditsBorderMon] ; scene
	and 3
	add a
	add a ; * 8
	add a
	ld e, a
	ld d, 0
	ld hl, CreditsPalettes
	add hl, de
	add hl, de ; * 3
	add hl, de
	ret

.UpdatePals
; Update the first three colors in both palette buffers.

	push af
	push hl
	add UnknBGPals % $100
	ld e, a
	ld a, 0
	adc UnknBGPals / $100
	ld d, a
	ld bc, 24
	rst CopyBytes

	pop hl
	pop af
	add BGPals % $100
	ld e, a
	ld a, 0
	adc BGPals / $100
	ld d, a
	ld bc, 24
	rst CopyBytes
	ret


CreditsPalettes:

; Pichu
	RGB 31, 00, 31
	RGB 31, 25, 00
	RGB 11, 14, 31
	RGB 07, 07, 07

	RGB 31, 05, 05
	RGB 11, 14, 31
	RGB 11, 14, 31
	RGB 31, 31, 31

	RGB 31, 05, 05
	RGB 00, 00, 00
	RGB 31, 31, 31
	RGB 31, 31, 31

; Smoochum
	RGB 31, 31, 31
	RGB 31, 27, 00
	RGB 26, 06, 31
	RGB 07, 07, 07

	RGB 03, 13, 31
	RGB 20, 00, 24
	RGB 26, 06, 31
	RGB 31, 31, 31

	RGB 03, 13, 31
	RGB 00, 00, 00
	RGB 31, 31, 31
	RGB 31, 31, 31

; Ditto
	RGB 31, 31, 31
	RGB 23, 12, 28
	RGB 31, 22, 00
	RGB 07, 07, 07

	RGB 03, 20, 00
	RGB 31, 22, 00
	RGB 31, 22, 00
	RGB 31, 31, 31

	RGB 03, 20, 00
	RGB 00, 00, 00
	RGB 31, 31, 31
	RGB 31, 31, 31

; Igglybuff
	RGB 31, 31, 31
	RGB 31, 10, 31
	RGB 31, 00, 09
	RGB 07, 07, 07

	RGB 31, 14, 00
	RGB 31, 00, 09
	RGB 31, 00, 09
	RGB 31, 31, 31

	RGB 31, 14, 00
	RGB 31, 31, 31
	RGB 31, 31, 31
	RGB 31, 31, 31

Credits_LoadBorderGFX:
	ld hl, wCreditsBorderFrame
	ld a, [hl]
	cp $ff
	jr z, .init

	and 3
	ld e, a
	inc a
	and 3
	ld [hl], a
	ld a, [wCreditsBorderMon]
	and 3
	add a
	add a
	add e
	add a
	ld e, a
	ld d, 0
	ld hl, .Frames
	add hl, de
	ld a, [hli]
	ld h, [hl]
	ld l, a
	ret

.init
	ld hl, wCreditsFaux2bpp
	ret

.Frames
	dw CreditsPichuGFX
	dw CreditsPichuGFX     + 16 tiles
	dw CreditsPichuGFX     + 32 tiles
	dw CreditsPichuGFX     + 48 tiles
	dw CreditsSmoochumGFX
	dw CreditsSmoochumGFX  + 16 tiles
	dw CreditsSmoochumGFX  + 32 tiles
	dw CreditsSmoochumGFX  + 48 tiles
	dw CreditsDittoGFX
	dw CreditsDittoGFX     + 16 tiles
	dw CreditsDittoGFX     + 32 tiles
	dw CreditsDittoGFX     + 48 tiles
	dw CreditsIgglybuffGFX
	dw CreditsIgglybuffGFX + 16 tiles
	dw CreditsIgglybuffGFX + 32 tiles
	dw CreditsIgglybuffGFX + 48 tiles

Credits_TheEnd:
	ld a, $40
	hlcoord 6, 9
	call .Load
	hlcoord 6, 10
.Load
	ld c, 8
.loop
	ld [hli], a
	inc a
	dec c
	jr nz, .loop
	ret

CreditsBorderGFX:    INCBIN "gfx/credits/border.2bpp"

CreditsMonsGFX:
CreditsPichuGFX:     INCBIN "gfx/credits/pichu.2bpp"
CreditsSmoochumGFX:  INCBIN "gfx/credits/smoochum.2bpp"
CreditsDittoGFX:     INCBIN "gfx/credits/ditto.2bpp"
CreditsIgglybuffGFX: INCBIN "gfx/credits/igglybuff.2bpp"


CreditsScript:

; Clear the banner.
	db CREDITS_CLEAR

; Pokemon Crystal Version Staff
	db   CREDITS_STAFF, 1

	db 	 CREDITS_WAIT, 8

; Play the credits music.
	db CREDITS_MUSIC

	db CREDITS_WAIT2, 10

	db CREDITS_WAIT, 1

; Update the banner.
	db CREDITS_SCENE, 0 ; Pichu

	db CREDITS_DIRECTOR, 1
	db   CREDITS_KOOLBOYMAN, 2

	db CREDITS_WAIT, 12

	db CREDITS_PROJECTMANAGER, 1
	db   CREDITS_KOOLBOYMAN, 2

	db CREDITS_WAIT, 12

	db CREDITS_PROGRAMMERS, 0
	db   CREDITS_KOOLBOYMAN, 1
	db   CREDITS_PIKALAX, 2
	db   CREDITS_ASDF, 3

	db CREDITS_WAIT, 12

	db CREDITS_PROGRAMMERS, 0
	db   CREDITS_PIGU, 1
	db   CREDITS_CHAUZU, 2
	db   CREDITS_CHAOS, 3

	db CREDITS_WAIT, 12

	db CREDITS_STORY, 0
	db   CREDITS_KOOLBOYMAN, 1
	db   CREDITS_LIAM, 2

	db CREDITS_WAIT, 12

	db CREDITS_GAMEPLAYMAPS, 0
	db   CREDITS_KOOLBOYMAN, 1
	db   CREDITS_LIGHTNING, 2

	db CREDITS_WAIT, 12

	db CREDITS_OVERWORLDSPRITES, 1
	db   CREDITS_MEGAMAN, 2

	db CREDITS_WAIT, 12

	db CREDITS_MUSICIANS, 0
	db   CREDITS_PIGU, 1
	db   CREDITS_SHANTYTOWN, 2
	db   CREDITS_LEVUSBEVUS, 3

	db CREDITS_WAIT, 12

	db CREDITS_MUSICIANS, 0
	db   CREDITS_CAT333POKEMON, 1
	db   CREDITS_CJ, 2
	db   CREDITS_GACT, 3

	db CREDITS_WAIT, 12

	db CREDITS_SOUNDDESIGN, 1
	db   CREDITS_PIGU, 2

	db CREDITS_WAIT, 12

	db CREDITS_MUSICIANS, 0
	db   CREDITS_CAT333POKEMON, 1
	db   CREDITS_CJ, 2
	db   CREDITS_SHANTYTOWN, 3

	db CREDITS_WAIT, 12

	db CREDITS_POKEMONDEMAKES, 0
	db   CREDITS_PIOXYS, 1
	db   CREDITS_BREEZEPLEEZE, 2

	db CREDITS_WAIT, 12

	db CREDITS_TRAINERDESIGNS, 0
	db   CREDITS_MEGAMAN, 1
	db   CREDITS_NSOMNIART, 2

	db CREDITS_WAIT, 12

	db CREDITS_NEWPOKEMON, 0
	db   CREDITS_NSOMNIART, 1
	db   CREDITS_ALLY, 2
	db   CREDITS_ERACLITO, 3

	db CREDITS_WAIT, 12

	db CREDITS_BATTLEANIMATIONS, 0
	db   CREDITS_PADZ, 1

	db CREDITS_WAIT, 12

	db CREDITS_TESTERS, 0
	db   CREDITS_ANTHONYLA, 1
	db   CREDITS_BRIAN, 2
	db   CREDITS_RYAN, 3
	db   CREDITS_CYBERSHELL, 4

	db CREDITS_WAIT, 12

	db CREDITS_TESTERS, 0
	db   CREDITS_DAN, 1
	db   CREDITS_RYANS, 2
	db   CREDITS_GAWERTY, 3
	db   CREDITS_JUSTIN, 4

	db CREDITS_WAIT, 12

	db CREDITS_TESTERS, 0
	db   CREDITS_JELLO, 1
	db   CREDITS_MAX, 2
	db   CREDITS_VINCE, 3

	db CREDITS_WAIT, 12

	db CREDITS_THANKS, 0
	db   CREDITS_GAMEFREAK, 1
	db   CREDITS_IIMARCKUS, 2
	db   CREDITS_REVO, 3

	db CREDITS_WAIT, 12

	db CREDITS_THANKS, 0
	db   CREDITS_TWITCH, 1
	db   CREDITS_YOU, 2

	db CREDITS_WAIT, 12

; Display "The End" graphic.
	db CREDITS_THEEND

	db CREDITS_WAIT, 20

	db CREDITS_END

CreditsStrings:
	dw .Koolboyman
	dw .Revo
	dw .Lightning
	dw .Pikalax
	dw .Chaos
	dw .Chauzu
	dw .Padz
	dw .Pioxys
	dw .Pret
	dw .FroggestSpirit
	dw .Asdf
	dw .Pigu
	dw .Streamer
	dw .DannyE
	dw .BlueEmerald
	dw .Chamber
	dw .BreezePleeze
	dw .Megaman
	dw .GACT
	dw .Liam
	dw .CJ
	dw .Ally
	dw .Nsomniart
	dw .Eraclito
	dw .Cat333Pokemon
	dw .LevusBevus
	dw .ShantyTown
	dw .AnthonyLa
	dw .Brian
	dw .Ryan
	dw .Cybershell
	dw .Dan
	dw .RyanS
	dw .Gawerty
	dw .Justin
	dw .Jello
	dw .Max
	dw .Vince
	dw .GameFreak
	dw .Twitch
	dw .You
	dw .IIMarckus

	dw .Staff
	dw .Director   
	dw .ProjectManager
	dw .Programmers
	dw .OverworldSprites
	dw .Music
	dw .SoundDesign
	dw .PokemonDemakes
	dw .TrainerDesigns
	dw .NewPokemon
	dw .GameplayMaps
	dw .Testers
	dw .Thanks
	dw .Story
	dw .BattleAnimations

.Koolboyman
	ctxt "Koolboyman"
	done
.Revo
	ctxt "ProjectRevoTPP"
	done
.Lightning
	ctxt "LightningXCE"
	done
.Pikalax
	ctxt "PikalaxALT"
	done
.Chaos
	ctxt "chaoslord2"
	done
.Chauzu
	ctxt "Chauzu VGC"
	done
.Padz
	text "Padz"
	done
.Pioxys
	text "Pioxys"
	done
.Pret
	text "pret"
	done
.FroggestSpirit
	ctxt "FroggestSpirit"
	done
.Asdf
	text "asdf14396"
	done
.Pigu
	text "Pigu"
	done
.Streamer
	ctxt "TwitchPlaysPokemon"
	done
.DannyE
	text "Danny-E 33"
	done
.BlueEmerald
	ctxt "Blue Emerald"
	done
.Chamber
	ctxt "Chamber"
	done
.BreezePleeze
	ctxt "BreezePleeze"
	done
.Megaman
	ctxt "megaman-omega"
	done
.GACT
	text "GACT"
	done
.Liam
	ctxt "Liam F."
	done
.CJ
	text "CJ M."
	done
.Ally
	ctxt "Ally G."
	done
.Nsomniart
	ctxt "nsomniart"
	done
.Eraclito
	ctxt "Eraclito"
	done
.Cat333Pokemon
	ctxt "Cat333Pokemon"
	done
.LevusBevus
	ctxt "Levi D."
	done
.ShantyTown
	ctxt "ShantyTown"
	done
;Testers
.AnthonyLa
	ctxt "Anthony La"
	done
.Brian
	ctxt "Brian D."
	done
.Ryan
	ctxt "Ryan S."
	done
.Cybershell
	ctxt "Cybershell12"
	done
.Dan
	text "Dan J."
	done
.RyanS
	ctxt "Ryan S."
	done
.Gawerty
	ctxt "Gawerty"
	done
.Justin
	ctxt "Justin K."
	done
.Jello
	ctxt "Jello"
	done
.Max
	ctxt "Max Hamee"
	done
.Vince
	ctxt "Vince Damon"
	done
.GameFreak
	ctxt "Game Freak"
	done
.Twitch
	ctxt "Twitch"
	done
.You
	ctxt "And You!"
	done
.IIMarckus
	text "IIMarckus"
	done

.Staff
	ctxt "      #mon"   
	next "       Prism" 
	next "       Staff"
	done
.Director
	ctxt "Director"
	done        
.ProjectManager
	ctxt "Project Manager"
	done
.Programmers
	ctxt "Programmers"
	done
.OverworldSprites
	ctxt "Overworld Sprites"
	done
.Music
	ctxt "Music"
	done
.SoundDesign
	ctxt "Sound Design"
	done
.PokemonDemakes
	ctxt "#mon Animations"
	done
.TrainerDesigns
	ctxt "Trainer Designs"
	done
.NewPokemon
	text "New #mon"
	done
.GameplayMaps
	ctxt "Gameplay & Maps"
	done
.Testers
	ctxt "Testers"
	done
.Thanks
	ctxt "Special Thanks"
	done
.Story
	ctxt "Story"
	done
.BattleAnimations
	ctxt "Move Animations"
	done

.Copyright
	;    (C) 1  9  9  5 - 2  0  0  1     N  i  n  t  e  n  d  o
	text $60,$61,$62,$63,$64,$65,$66, $67, $68, $69, $6a, $6b, $6c
	;    (C) 1  9  9  5 - 2  0  0  1    C  r  e  a  t  u  r  e  s      i  n  c .
	next $60,$61,$62,$63,$64,$65,$66, $6d, $6e, $6f, $70, $71, $72,  $7a, $7b, $7c
	;    (C) 1  9  9  5 - 2  0  0  1  G   A   M   E   F   R   E   A   K     i  n  c .
	next $60,$61,$62,$63,$64,$65,$66, $73, $74, $75, $76, $77, $78, $79,  $7a, $7b, $7c
	done
