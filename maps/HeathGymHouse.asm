HeathGymHouse_MapScriptHeader;trigger count
	db 0
 ;callback count
	db 0

HeathGymHouse_MapEventHeader ;filler
	db 0, 0

;warps
	db 3
	warp_def $9, $3, 2, HEATH_GYM
	warp_def $9, $4, 2, HEATH_GYM
	warp_def $5, $7, 2, HEATH_GYM_UNDERGROUND

	;xy triggers
	db 0

	;signposts
	db 0

	;people-events
	db 1
	person_event SPRITE_POKE_BALL, 3, 3, SPRITEMOVEDATA_ITEM_TREE, 0, 0, -1, -1, PAL_OW_RED, 3, TM_RAGE, 0, EVENT_GET_TM57
